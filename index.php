<?php
/*
  * @package  Joomla.Site
  * @subpackage  Templates.UAB_5.0Linear
  * @copyright   2019 UAB - The University of Alabama at Birmingham. All rights reserved.
  * 01-30-30
 */
defined('_JEXEC') or die;
$this->setTitle( $this->getTitle() . ' | ' . 'UAB' );
$app				= JFactory::getApplication();
$doc				= JFactory::getDocument(); unset($doc->base);
$params				= JFactory::getApplication()->getTemplate(true)->params;
$bodyClass			= $params->get( 'bodyClass' );
$hd 				= $params->get( 'hd' ); //legacy var for upgrade compatiblity, also in body
$layout				= $params->get( 'layout' );
$logoType			= $params->get( 'logoType' );
$logoPathOther		= $params->get( 'logoPathOther' );
$othercss			= $params->get( 'othercss' );
$ribbonColor		= $params->get( 'ribbonColor' );
$ribbonPosition		= $params->get( 'ribbonPosition' );
$footerColor		= $params->get( 'footerColor' );
$siteNameSub		= $params->get( 'siteNameSub' );
$siteNameTop		= $params->get( 'siteNameTop' );
$sitename			= $app->getCfg( 'sitename' );
$itemid				= $app->input->getCmd( 'Itemid' , '' );
$menu				= $app->getMenu();
$menu 				= $app->getMenu()->getActive();
$pageclass			= ''; if (is_object($menu)) $pageclass = $menu->params->get( 'pageclass_sfx' );
$loc				= (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" . $_SERVER['HTTP_HOST']; 
$base				= JURI::base(); 
$uri				= &JURI::getInstance();

// Unset jquery scripts and bootstrap on load

$headData = $doc->getHeadData();
$scripts = $headData['scripts'];

//scripts and styles to remove, customise as required

unset($scripts[JUri::root(true) . '/media/jui/js/jquery.min.js']);
unset($scripts[JUri::root(true) . '/media/jui/js/jquery-noconflict.js']);
unset($scripts[JUri::root(true) . '/media/jui/js/bootstrap.min.js']);
unset($scripts[JUri::root(true) . '/media/jui/js/jquery-migrate.min.js']);

$headData['scripts'] = $scripts;
$doc->setHeadData($headData);

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">

<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-KH9W5DS');</script>
    <!-- End Google Tag Manager -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <meta property="og:type" content="website" />
    <meta name="monsido" content="monsido" />

    
   <!-- Minifying JS scripts and loading jquery and bootstrap js css from a CDN -->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
   <script type="text/javascript" src="/styles/5.0-speed/js/scripts.min.js" defer></script>
   <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" async></script> -->
  
    <?php if($this->params->get('layout') != ('linear hd')) : ?>
    <link rel="stylesheet" href="//use.typekit.net/qyx1hma.css">
    <?php endif; ?>
    
    <link rel="shortcut icon" href="/styles/5.0-speed/images/favicons/favicon.ico">
    <link rel="apple-touch-icon" sizes="57x57" href="/styles/5.0-speed/images/favicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/styles/5.0-speed/images/favicons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/styles/5.0-speed/images/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/styles/5.0-speed/images/favicons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/styles/5.0-speed/images/favicons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/styles/5.0-speed/images/favicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/styles/5.0-speed/images/favicons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/styles/5.0-speed/images/favicons/apple-touch-icon-152x152.png">
    <link rel="icon" sizes="196x196" type="image/png" href="/styles/5.0-speed/images/favicons/favicon-196x196.png">
    <link rel="icon" sizes="160x160" type="image/png" href="/styles/5.0-speed/images/favicons/favicon-160x160.png">
    <link rel="icon" sizes="96x96" type="image/png" href="/styles/5.0-speed/images/favicons/favicon-96x96.png">
    <link rel="icon" sizes="16x16" type="image/png" href="/styles/5.0-speed/images/favicons/favicon-16x16.png">
    <link rel="icon" sizes="32x32" type="image/png" href="/styles/5.0-speed/images/favicons/favicon-32x32.png">
    <meta name="msapplication-TileColor" content="#1e6b52">
    <meta name="msapplication-TileImage" content="/styles/5.0-speed/images/favicons/mstile-144x144.png">
    <meta name="msapplication-square70x70logo" content="/styles/5.0-speed/images/favicons/mstile-70x70.png">
    <meta name="msapplication-square144x144logo" content="/styles/5.0-speed/images/favicons/mstile-144x144.png">
    <meta name="msapplication-square150x150logo" content="/styles/5.0-speed/images/favicons/mstile-150x150.png">
    <meta name="msapplication-square310x310logo" content="/styles/5.0-speed/images/favicons/mstile-310x310.png">
    <meta name="msapplication-wide310x150logo" content="/styles/5.0-speed/images/favicons/mstile-310x150.png">
    
    <jdoc:include type="modules" name="meta" style="raw" />
    <jdoc:include type="head" />

    <?php
	// Add JavaScript Frameworks
	// JHtml::_('bootstrap.framework');
	// Add Stylesheets
	JHtmlBootstrap::loadCss();
	// Load optional rtl Bootstrap css and Bootstrap bugfixes
	JHtmlBootstrap::loadCss($includeMaincss = false, $this->direction);
	?>

    <link rel="stylesheet" media="all" href="<?php echo $this->baseurl ?>/templates/uab5.0-speed/TYLER-css/uab-template.css" type="text/css" />

    <link rel="stylesheet" media="all" href="<?php echo $this->baseurl ?>/templates/uab5.0-speed/TYLER-css/ur-template.css" type="text/css" />

<!--
   <?php if(strpos($bodyClass, 'dept') !== false) : ?>
   
    <link rel="stylesheet" media="all" href="/styles/5.0-speed/css/dept.css" type="text/css" /> 
    <?php endif; ?> 

    <?php if(strpos($bodyClass, 'center') !== false) : ?>
    <link rel="stylesheet" media="all" href="/styles/5.0-speed/css/ur-templates/ur--center-template.css" type="text/css" />
    <?php endif; ?>

    <?php if(strpos($bodyClass, 'department') !== false) : ?>
    <link rel="stylesheet" media="all" href="/styles/5.0-speed/css/ur-templates/ur--department-template.css" type="text/css" />
    <?php endif; ?>

    <?php if(strpos($bodyClass, 'base') !== false) : ?>
    <link rel="stylesheet" media="all" href="/styles/5.0-speed/css/ur-templates/ur--base-styles.css" type="text/css" />
    <?php endif; ?>

    <?php if(strpos($bodyClass, 'template-') !== false) : ?>
    <?php $tempCheck = substr($bodyClass, strpos($bodyClass, "template")); $tempNum =  substr($tempCheck,0,strpos($tempCheck," ")); ?>
    <link rel="stylesheet" media="all" href="/styles/5.0-speed/css/ur-templates/<?php echo $tempNum;?>.css" type="text/css" />
    <?php endif ; ?> 
-->


    <?php if ($this->params->get('group') == ('1')) : ?>
    <?php
		$snip1=preg_replace("/^\//","",JURI::base(true));
		$snip2=preg_replace("/\/.*/","",$snip1);
		$snip3=preg_replace("/^/","/",$snip2);
		?>
    <link rel="stylesheet" media="all" href="/styles/5.0-speed/css/groups<?php echo $snip3; ?>_group.css" type="text/css" />
    <?php endif; ?>

    <?php if ($this->params->get('group' ) == ('3')) : ?>
    <link rel="stylesheet" media="all" href="<?php echo htmlspecialchars("$othercss"); ?>" type="text/css" />
    <?php endif; ?>

    <?php $spec = "site_specific.css";	?>
    <link rel="stylesheet" media="all" href="<?php echo $this->baseurl . '/' . $spec . '?v=' . filemtime($spec); ?>" type="text/css" />

    <?php if ($this->params->get( 'sitescripts' )) : ?>
    <script type="text/javascript" src="<?php echo $this->baseurl ?>/site_scripts.js" defer></script>
    <?php endif; ?>
    <?php if ($this->params->get( 'groupscripts' )) : ?>
    <script type="text/javascript" src="/styles/5.0-speed/js/groups<?php echo $snip3; ?>_group.js" defer></script>
    <?php endif; ?>

</head>

<body id="<?php echo ($itemid ? 'itemid-' . $itemid : ''); ?>" class="<?php echo htmlspecialchars("$hd $layout $ribbonColor $ribbonPosition $bodyClass $pageclass $footerColor"); ?><?php if($this->countModules( 'sidebar or right or left' )) : ?> sidebars<?php endif; ?><?php if($this->countModules( 'unit' )) : ?> unit<?php endif; ?><?php if($this->countModules( 'alert' )) : ?> alertActive<?php endif; ?>">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KH9W5DS"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <?php if($this->countModules( 'ribbon' )) : ?>
    <a tabindex="0" class="skip-main" href="#ribbon" aria-expanded="false"><span>Skip to navigation</span></a>
    <?php endif; ?>
    <a tabindex="0" class="skip-main" href="#content" aria-expanded="false"><span>Skip to main content</span></a>
    <div id="page_bg" class="<?php echo $this->params->get('backgroundVariation'); ?>"> </div>
    <!--page_bg ends -->
    <?php if($this->params->get('layout') == ('linear hd')) : ?>
    <div class="topLinearContainer"><a href="https://www.uab.edu/home" aria-label="Link to the UAB homepage" title="UAB Logo">
            <div class="topLinearLogo"></div>
        </a></div>
    <?php endif; ?>
    <?php if($this->countModules( 'alert' )) : ?>
    <!-- SYSTEM ALERT BOX ABOVE LOGO AND WHITEBAR -->
    <div id="alert">
        <jdoc:include type="modules" name="alert" style="xhtml" />
    </div>
    <?php endif; ?>

    <!-- ADMIN LINKS BEGIN -->
    <div id="hidden-links">
        <a title="administrator login" class="un_jtt_hide" href="<?php echo $this->baseurl ?>/administrator">administrator login</a>
        <a title="frontend login" class="un_jtt_hide" href="<?php echo $this->baseurl ?>/login" style="position:absolute;right:0;">frontend login</a>
    </div>

    <!-- WHITEBAR BACKGROUND -->
    <?php if($this->params->get('layout') != ('linear hd')) : ?>
    <?php if($this->countModules( 'unit' )) : ?>
    <div id="unit-whitebar"></div>
    <div id="unit-greenbar"></div>
    <?php else: ?>
    <div id="winston-whitebar">&nbsp;</div>
    <?php endif; ?>
    <?php endif; ?>
    <div id="wrapper" class="<?php echo $ribbonPosition; ?> <?php echo $this->params->get('backgroundVariation'); ?>">
        <!-- BEGIN wrapper -->

        <?php if(strpos($bodyClass, 'hideWhitebar') !== false ) : ?>
        <?php elseif(empty($siteNameTop) && empty($this->countModules( 'search or quicklinks' ))) : ?>
        <?php else : ?>
        <div id="whitebar">
            <!-- BEGIN WHITEBAR -->
            <?php if($this->params->get('layout') != ('linear hd')) : ?>
            <div id="logo">
                <!-- BEGIN LOGO -->
                <?php if($this->params->get('logoPath') == ('default')) : ?>
                <a aria-label="link to homepage of this site" href="<?php echo $this->baseurl; ?>">
                    <?php else : ?>
                    <a aria-label="link to homepage of this site" href="<?php echo $this->params->get('logoPathOther'); ?>">
                        <?php endif; ?>
                        <?php if ($this->params->get( 'logoType' )) : ?>
                        <img src="<?php echo $this->baseurl ?>/sitelogo.jpg" alt="Site logo" />
                        <?php else: ?>
                        <img src="//www.uab.edu/styles/images/sitelogo.jpg" alt="UAB logo" />
                        <?php endif; ?>
                    </a>
            </div><!-- END LOGO -->
            <?php elseif($this->params->get('siteNameTop')) : ?>
            <div id="siteName">
                <?php if($this->params->get('logoPath') == ('default')) : ?>
                <a href="<?php echo $this->baseurl; ?>">
                    <?php else : ?>
                    <a href="<?php echo $this->params->get('logoPathOther'); ?>">
                        <?php endif; ?>
                        <?php if($this->params->get('siteNameSub')) : ?>
                        <?php echo '<div class="siteNameTop">' . htmlspecialchars($this->params->get('siteNameTop'), ENT_COMPAT, 'UTF-8') . '</div>'; ?>
                        <?php else : ?>
                        <?php echo '<div class="siteNameTop noSub">' . htmlspecialchars($this->params->get('siteNameTop'), ENT_COMPAT, 'UTF-8') . '</div></a>'; ?>
                        <?php endif; ?>
                        <?php if($this->params->get('siteNameSub')) : ?>
                        <?php echo '<div class="siteNameSub">' . htmlspecialchars($this->params->get('siteNameSub'), ENT_COMPAT, 'UTF-8') . '</div></a>'; ?>
                        <?php endif; ?>
            </div><!-- END siteName -->
            <?php endif; ?>
            <?php if($this->countModules( 'search or quicklinks' )) : ?>
            <div class="searchpanel">
                <div class="searchpanel_content">
                    <!-- BEGIN searchpanelcontent -->
                    <?php if($this->countModules( 'search' )) : ?>
                    <div id="search">
                        <jdoc:include type="modules" name="search" style="xhtml" />
                    </div>
                    <?php endif; ?>
                    <?php if($this->countModules( 'quicklinks' )) : ?>
                    <div id="quicklinks">
                        <jdoc:include type="modules" name="quicklinks" style="xhtml" />
                    </div>
                    <?php endif; ?>
                </div><!-- END searchpanelcontent -->
            </div><!-- END searchpanel -->
            <a id="trigger" href="#" aria-label="trigger to open ribbon menu on smaller screens" title="ribbon menu dropdown trigger">&nbsp;</a>
            <?php endif; ?>
        </div> <!-- END WHITEBAR -->
        <?php endif; ?>

        <?php if($this->countModules( 'unit' )) : ?>
        <!-- UNIT -->
        <div id="unit">
            <jdoc:include type="modules" name="unit" style="xhtml" />
        </div>
        <?php endif; ?>
        <div style="clear:both"></div>

        <?php if($this->countModules( 'ribbon' ) && $ribbonPosition == ('ribbonFullwidth')) : ?>
        <!-- RIBBON Fullwidth -->

        <nav class="navbar navbar-static-top" role="presentation" aria-label="main navigation">
            <div id="tray" class="<?php echo ("$ribbonColor $ribbonPosition"); ?> navbar-inner">
                <div id="ribbon" class="container">
                    <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <!-- Everything you want hidden at 940px or less, place within here -->
                    <div class="nav-collapse collapse">
                        <jdoc:include type="modules" name="ribbon" style="xhtml" />
                    </div>
                </div>
            </div>
        </nav>

        <?php endif; ?>

        <?php if($this->countModules( 'banner' )) : ?>
        <!-- BANNER -->
        <div id="banner" role="banner" class="main-banner">
            <jdoc:include type="modules" name="banner" style="xhtml" />
        </div>
        <?php endif; ?>
        <div style="clear:both"></div>

        <?php if($this->countModules( 'ribbon' ) && $ribbonPosition == ('ribbonClassic')) : ?>
        <!-- RIBBON Classic -->

        <nav role="navigation" class="navbar navbar-static-top">
            <div id="tray" class="<?php echo $this->params->get('ribbonColor'); ?> <?php echo $this->params->get('ribbonPosition'); ?> navbar-inner">
                <div id="ribbon" class="container">
                    <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <!-- Everything you want hidden at 940px or less, place within here -->
                    <div class="nav-collapse collapse">
                        <jdoc:include type="modules" name="ribbon" style="xhtml" />
                    </div>
                </div>
            </div>
        </nav>

        <?php endif; ?>

        <div style="clear:both"></div>

        <!-- MAIN PAGE AREA (WHITEBOX) -->
        <div role="main" id="whitebox" class="<?php echo $this->params->get('colorVariation'); ?>">

            <jdoc:include type="message" />

            <?php if($this->countModules( 'sidebar or left' )) : ?>
            <!-- LEFT COLUMN -->
            <div id="leftcolumn" class="sidebarpanel <?php echo $this->params->get('sidebarVariation'); ?>">

                <?php if($this->countModules( 'sidebar' )) : ?>
                <!-- SIDEBAR MENU -->
                <div class="sidebar <?php echo $this->params->get('sidebarVariation'); ?> sidebarpanel_content">
                    <jdoc:include type="modules" name="sidebar" style="xhtml" />
                </div>
                <?php endif; ?>

                <?php if($this->countModules( 'left' )) : ?>
                <!-- LEFT POSITION -->
                <div class="left-position sidebarpanel_content">
                    <jdoc:include type="modules" name="left" style="xhtml" />
                </div>
                <?php endif; ?>

            </div>
            <a id="sidebar_trigger" aria-label="trigger to open sidebar menu on smaller screens" class="<?php echo $this->params->get('sidebarVariation'); ?>" href="#">
                <div class="sidebar_trigger_title">More</div>
            </a>
            <?php endif; ?>

            <!-- MAIN COLUMN -->
            <?php if($this->countModules( 'sidebar and right' )) : ?>
            <div id="maincolumn-l-r">
                <?php elseif($this->countModules( 'left and right' )) : ?>
                <div id="maincolumn-l-r">
                    <?php elseif($this->countModules( 'left or sidebar' )) : ?>
                    <div id="maincolumn-l">
                        <?php elseif($this->countModules( 'right' )) : ?>
                        <div id="maincolumn-r">
                            <?php else: ?>
                            <div id="maincolumn-wide">
                                <?php endif; ?>

                                <?php if($this->countModules( 'message' )) : ?>
                                <!-- SYSTEM / EMERGENCY MESSAGE -->
                                <div id="message">
                                    <jdoc:include type="modules" name="message" style="xhtml" />
                                </div>
                                <?php endif; ?>

                                <?php if ($this->countModules( 'breadcrumbs' )) : ?>
                                <!-- BREADCRUMBS -->
                                <div id="breadcrumbs">
                                    <jdoc:include type="modules" name="breadcrumbs" style="xhtml" />
                                </div>
                                <?php endif; ?>

                                <?php if($this->countModules( 'upper1 or upper2 or upper3' )) : ?>
                                <!-- UPPER CONTAINER -->
                                <div class="uppers">
                                    <?php if($this->countModules( 'upper1 and upper2 and upper3' )) : ?>
                                    <!-- CONDITIONAL UPPERS-->
                                    <div class="upper1 third">
                                        <jdoc:include type="modules" name="upper1" style="xhtml" />
                                    </div>
                                    <div class="upper2 third">
                                        <jdoc:include type="modules" name="upper2" style="xhtml" />
                                    </div>
                                    <div class="upper3 third">
                                        <jdoc:include type="modules" name="upper3" style="xhtml" />
                                    </div>

                                    <?php elseif($this->countModules( 'upper1 and upper2' )) : ?>
                                    <div class="upper1 half">
                                        <jdoc:include type="modules" name="upper1" style="xhtml" />
                                    </div>
                                    <div class="upper2 half">
                                        <jdoc:include type="modules" name="upper2" style="xhtml" />
                                    </div>

                                    <?php elseif($this->countModules( 'upper2 and upper3' )) : ?>
                                    <div class="upper2 half">
                                        <jdoc:include type="modules" name="upper2" style="xhtml" />
                                    </div>
                                    <div class="upper3 half">
                                        <jdoc:include type="modules" name="upper3" style="xhtml" />
                                    </div>

                                    <?php elseif($this->countModules( 'upper1 and upper3' )) : ?>
                                    <div class="upper1 half">
                                        <jdoc:include type="modules" name="upper1" style="xhtml" />
                                    </div>
                                    <div class="upper3 half">
                                        <jdoc:include type="modules" name="upper3" style="xhtml" />
                                    </div>

                                    <?php elseif($this->countModules( 'upper1' )) : ?>
                                    <div class="upper1 full">
                                        <jdoc:include type="modules" name="upper1" style="xhtml" />
                                    </div>

                                    <?php elseif($this->countModules( 'upper2' )) : ?>
                                    <div class="upper2 full">
                                        <jdoc:include type="modules" name="upper2" style="xhtml" />
                                    </div>

                                    <?php elseif($this->countModules( 'upper3' )) : ?>
                                    <div class="upper3 full">
                                        <jdoc:include type="modules" name="upper3" style="xhtml" />
                                    </div>
                                    <?php endif; ?>
                                </div>
                                <?php endif; ?>

                                <?php if ($this->countModules( 'full1' )) : ?>
                                <!-- FULL1 -->
                                <div class="full full1">
                                    <jdoc:include type="modules" name="full1" style="xhtml" />
                                </div>
                                <?php endif; ?>

                                <?php if($this->countModules( 'uppermiddle1 or uppermiddle2' )) : ?>
                                <!-- UPPERMIDDLE CONTAINER -->
                                <div class="uppermiddles">
                                    <div class="uppermiddle1 two_thirds">
                                        <jdoc:include type="modules" name="uppermiddle1" style="xhtml" />
                                    </div>
                                    <div class="uppermiddle2 third">
                                        <jdoc:include type="modules" name="uppermiddle2" style="xhtml" />
                                    </div>
                                </div>
                                <?php endif; ?>

                                <?php if ($this->countModules( 'full2' )) : ?>
                                <!-- FULL2 -->
                                <div class="full full2">
                                    <jdoc:include type="modules" name="full2" style="xhtml" />
                                </div>
                                <?php endif; ?>

                                <?php if($this->countModules( 'uppermiddle3 or uppermiddle4' )) : ?>
                                <!-- UPPERMIDDLE CONTAINER -->
                                <div class="uppermiddles">
                                    <div class="uppermiddle3 third">
                                        <jdoc:include type="modules" name="uppermiddle3" style="xhtml" />
                                    </div>
                                    <div class="uppermiddle4 two_thirds">
                                        <jdoc:include type="modules" name="uppermiddle4" style="xhtml" />
                                    </div>
                                </div>
                                <?php endif; ?>

                                <!-- COMPONENT AREA -->
                                <div id="content" class="<?php echo $this->params->get('colorVariation'); ?>">
                                    <jdoc:include type="component" />
                                </div>

                                <?php if ($this->countModules( 'full3' )) : ?>
                                <!-- FULL3 -->
                                <div class="full full3">
                                    <jdoc:include type="modules" name="full3" style="xhtml" />
                                </div>
                                <?php endif; ?>

                                <?php if($this->countModules( 'middle1 or middle2' )) : ?>
                                <!-- MIDDLE CONTAINER -->
                                <div class="middles">
                                    <div class="middle1 two_thirds">
                                        <jdoc:include type="modules" name="middle1" style="xhtml" />
                                    </div>
                                    <div class="middle2 third">
                                        <jdoc:include type="modules" name="middle2" style="xhtml" />
                                    </div>
                                </div>
                                <?php endif; ?>

                                <?php if ($this->countModules( 'full4' )) : ?>
                                <!-- FULL4 -->
                                <div class="full full4">
                                    <jdoc:include type="modules" name="full4" style="xhtml" />
                                </div>
                                <?php endif; ?>

                                <?php if($this->countModules( 'middle3 or middle4' )) : ?>
                                <!-- MIDDLE CONTAINER -->
                                <div class="middles">
                                    <div class="middle3 third">
                                        <jdoc:include type="modules" name="middle3" style="xhtml" />
                                    </div>
                                    <div class="middle4 two_thirds">
                                        <jdoc:include type="modules" name="middle4" style="xhtml" />
                                    </div>
                                </div>
                                <?php endif; ?>

                                <?php if ($this->countModules( 'full5' )) : ?>
                                <!-- FULL5 -->
                                <div class="full full5">
                                    <jdoc:include type="modules" name="full5" style="xhtml" />
                                </div>
                                <?php endif; ?>

                                <?php if($this->countModules( 'lower1 or lower2 or lower3' )) : ?>
                                <!-- LOWER CONTAINER -->
                                <div class="lowers">
                                    <?php if($this->countModules( 'lower1 and lower2 and lower3' )) : ?>
                                    <!-- CONDITIONAL LOWERS-->
                                    <div class="lower1 third">
                                        <jdoc:include type="modules" name="lower1" style="xhtml" />
                                    </div>
                                    <div class="lower2 third">
                                        <jdoc:include type="modules" name="lower2" style="xhtml" />
                                    </div>
                                    <div class="lower3 third">
                                        <jdoc:include type="modules" name="lower3" style="xhtml" />
                                    </div>

                                    <?php elseif($this->countModules( 'lower1 and lower2' )) : ?>
                                    <div class="lower1 half">
                                        <jdoc:include type="modules" name="lower1" style="xhtml" />
                                    </div>
                                    <div class="lower2 half">
                                        <jdoc:include type="modules" name="lower2" style="xhtml" />
                                    </div>

                                    <?php elseif($this->countModules( 'lower2 and lower3' )) : ?>
                                    <div class="lower2 half">
                                        <jdoc:include type="modules" name="lower2" style="xhtml" />
                                    </div>
                                    <div class="lower3 half">
                                        <jdoc:include type="modules" name="lower3" style="xhtml" />
                                    </div>

                                    <?php elseif($this->countModules( 'lower1 and lower3' )) : ?>
                                    <div class="lower1 half">
                                        <jdoc:include type="modules" name="lower1" style="xhtml" />
                                    </div>
                                    <div class="lower3 half">
                                        <jdoc:include type="modules" name="lower3" style="xhtml" />
                                    </div>

                                    <?php elseif($this->countModules( 'lower1' )) : ?>
                                    <div class="lower1 full">
                                        <jdoc:include type="modules" name="lower1" style="xhtml" />
                                    </div>

                                    <?php elseif($this->countModules( 'lower2' )) : ?>
                                    <div class="lower2 full">
                                        <jdoc:include type="modules" name="lower2" style="xhtml" />
                                    </div>

                                    <?php elseif($this->countModules( 'lower3' )) : ?>
                                    <div class="lower3 full">
                                        <jdoc:include type="modules" name="lower3" style="xhtml" />
                                    </div>
                                    <?php endif; ?>
                                </div>
                                <?php endif; ?>

                                <?php if ($this->countModules( 'full6' )) : ?>
                                <!-- FULL6 -->
                                <div class="full full6">
                                    <jdoc:include type="modules" name="full6" style="xhtml" />
                                </div>
                                <?php endif; ?>

                                <?php if($this->countModules( 'syndicate' )) : ?>
                                <!-- SYNDICATE AREA  -->
                                <div id="syndicate">
                                    <jdoc:include type="modules" name="syndicate" style="xhtml" />
                                </div>
                                <?php endif; ?>


                                <!-- TESTING FORM ASSEMBLY EMBEDS -->

                                <?php
                                
                                //Set stream options
                                $context = stream_context_create(array('http' => array('ignore_errors' => true)));
                                if(!isset($_GET['tfa_next'])) {
                                $qs = ' ';
                                if(isset($_SERVER['QUERY_STRING']) && !empty($_SERVER['QUERY_STRING'])){$qs='?'.$_SERVER['QUERY_STRING'];};
                                echo file_get_contents('https://uab.tfaforms.net/rest/forms/view/217886'.$qs);
                                } else {
                                echo file_get_contents('https://app.formassembly.com/rest'.$_GET['tfa_next'],false,$context);
                                }
                                ?>

                                <hr />

                                <?php
                                //Set stream options
                                $context = stream_context_create(array('http' => array('ignore_errors' => true)));
                                if(!isset($_GET['tfa_next'])) {
                                $qs = ' ';
                                if(isset($_SERVER['QUERY_STRING']) && !empty($_SERVER['QUERY_STRING'])){$qs='?'.$_SERVER['QUERY_STRING'];};
                                echo file_get_contents('https://uab.tfaforms.net/rest/forms/view/217721'.$qs);
                                } else {
                                echo file_get_contents('https://app.formassembly.com/rest'.$_GET['tfa_next'],false,$context);
                                } 
                                ?>
                                
                            </div>
                            <!-- 		</div> CONTENT ENDS -->
                            <?php if($this->countModules( 'right' )) : ?>

                            <?php if($this->countModules( 'sidebar <= right or left <= right' )) : ?>
                            <div id="downright" class="<?php echo $this->params->get('colorVariation'); ?>">
                                <div id="rightsidebar" class="<?php if ($this->params->get( 'rightsidebarVariation' )) : ?>transparent<?php endif; ?> respo">
                                    <jdoc:include type="modules" name="right" style="xhtml" />
                                </div>
                                <?php else: ?>
                                <div id="rightsidebar" class="<?php if ($this->params->get( 'rightsidebarVariation' )) : ?>transparent<?php endif; ?> non-respo">
                                    <jdoc:include type="modules" name="right" style="xhtml" />
                                </div>
                                <?php endif; ?>

                                <?php endif; ?>
                            </div><!-- DOWNRIGHT ENDS -->

                            <?php if($this->countModules( 'epilogue' )) : ?>
                            <!-- EPILOGUE AREA  -->
                            <div id="epilogue" class="<?php echo $this->params->get('colorVariation'); ?>">
                                <jdoc:include type="modules" name="epilogue" style="xhtml" />
                            </div>
                            <?php endif; ?>
                        </div> <!--  WHITEBOX ENDS -->
                        <!-- BEGIN LEGACY FOOTER -->
                        <div id="footer" class="<?php echo $this->params->get('colorVariation'); ?> <?php echo $this->params->get('ribbonPosition'); ?> <?php echo $this->params->get('footerColor'); ?>">
                            <div style="clear:both">&nbsp;</div>
                            <div id="footer_required">
                                <jdoc:include type="modules" name="footer" style="xhtml" />
                            </div>
                        </div>
                        <!-- END LEGACY FOOTER -->
                        <?php if($this->params->get('layout') == ('linear hd')) : ?>
                        <!-- BEGIN LOCKED FOOTER, LINEAR HD ONLY -->
                        <div role="contentinfo" id="lockedFooter" class="<?php echo $this->params->get('colorVariation'); ?> <?php echo $this->params->get('ribbonPosition'); ?>  <?php echo $this->params->get('footerColor'); ?>">
                            <ul>
                                <li><a href="https://assistive.usablenet.com/tt/<?php echo $uri ?>" target="_blank">Text-only</a></li>
                                <li class="lockedFooterSeparator">|</li>
                                <li><a href="https://www.uab.edu/toolkit/web/privacy" target="_blank">Privacy</a></li>
                                <li class="lockedFooterSeparator">|</li>
                                <li><a href="https://www.uab.edu/toolkit/web/terms-of-use" target="_blank">Terms of Use</a></li>
                                <li class="lockedFooterSeparator">|</li>
                                <li>&copy; <?php echo date('Y'); ?> The University of Alabama at Birmingham</li>
                            </ul>
                            <div class="eeoDisclaimer" style="max-width: 972px;">UAB is an Equal Opportunity/Affirmative Action Employer committed to fostering a diverse, equitable and family-friendly environment in which all faculty and staff can excel and achieve work/life balance irrespective of race, national origin, age, genetic or family medical history, gender, faith, gender identity and expression as well as sexual orientation. UAB also encourages applications from individuals with disabilities and veterans.</div>
                        </div> <!-- END LOCKED FOOTER, LINEAR HD ONLY -->
                        <?php endif; ?>
                    </div> <!--  WRAPPER ENDS -->

</body>

<jdoc:include type="modules" name="debug" style="raw" />

</html>